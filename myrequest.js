var axios = require('axios') //module pour faire GET/POST externe
    , https = require('https')
;


// PACKAGES
// axios : https://www.npmjs.com/package/axios MIT License
// node-cache: https://www.npmjs.com/package/node-cache

/**
 * AXIOS: http://zetcode.com/javascript/axios/
 * https://github.com/axios/axios
 */

//API Proximity
let api = "https://api.gogowego.com/v1/Messages";

let headers = (token) => {
    let str1 = 'Bearer';
    let str2 = str1.concat(' ', token);
    return {
        'Authorization': str2,
        'Content-Type': 'application/json'
    };
};

let agent = new https.Agent({
    rejectUnauthorized: false
});

let config = (method, url, headers, data = {}) => {
    return {
        method: method,
        url: url,
        headers : headers,
        data: data,
        httpsAgent: agent
    };
}

let configBotParty = ( token , message ) => {
    let dataBotParty = ( message ) => {
      return {
        "text": message,
        "userId":"userId",
        "platform":"proximity"
      }
    };
    return config( 'POST', api, headers(token), dataBotParty(message) );
};

let sendMessageToBotParty = async ( token, message ) => {
    try {
        let res = await axios(configBotParty(token, message));
        if (res.status=='200' || res.status=='201' ){
            console.log(res.data);
            return res.data;
        } else {
            console.log(res);
        }
    } catch (error) {
        console.error(error);
    }
};


module.exports = { sendMessageToBotParty
};
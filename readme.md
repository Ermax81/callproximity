Simple bout de code pour tester l'appel à l'API Message de Proximity.

Pour plus d'information sur Proximity: https://www.gogowego.com/

Démarrer le serveur NodeJS: node server.js

Faire une requête post sur le serveur (voir le fichier exemple test.sh)
en passant un json du type suivant:
{"token":"d3387c09-0c2a-4e7c-8a25-da8e772e0c6a","message":"Bonjour"}
où token est l'identifiant du chatbot à contacter, et message le texte à envoyer au chatbot.

Exemple de requête: 
    
    curl -v -k -X POST http://localhost:8080/ -H "Content-Type: application/json" -d '{"token":"d3387c09-0c2a-4e7c-8a25-da8e772e0c6a","message":"Bonjour"}'
    
Réponse: 

    { id: 'AsPmzODaDjM8vZcgwl0ONRYW37W',
      timestamp: '2020-02-05T16:22:31+00:00',
      sessionId: 'bAzI9291CC',
      query: 'Bonjour',
      contexts: [],
      status: { code: 200, errorType: 'success' },
      text: 'Bienvenue ',
      textToSpeech: { type: 'plainText', value: 'Bienvenue ' },
      fulfillment: 
       [ { source: 'testBotApi', sourceId: 'fxbFPvb5J1', items: [Array] } ],
      intents: 
       [ { source: 'testBotApi',
           name: 'Welcome Intent',
           confidence: 1,
           isFallback: false,
           endConversation: false,
           slots: {} } ],
      conversationId: '4YSf81lMKq' }


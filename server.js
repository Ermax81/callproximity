var express = require('express')          //Fast, unopinionated, minimalist web framework for node
    , app     = express()
    , version = process.versions
    , bodyParser = require('body-parser') //Node.js body parsing middleware
    , myrequest = require("./myrequest")
    , axios = require('axios') //module pour faire GET/POST externe
;

// PACKAGES
// express: https://www.npmjs.com/package/express MIT License
// body-parer : https://www.npmjs.com/package/body-parser MIT License
// axios : https://www.npmjs.com/package/axios MIT License

// parse application/json
app.use(bodyParser.json());

var appName = process.env.APP_NAME || 'api'
    , port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080
    , ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'
;

if (process.env[appName.toUpperCase() + '_SERVICE_HOST'] ==  null){
  ip = '127.0.0.1';
}

console.log('---ENV---');
console.log(process.env);
console.log('---CHECK---');
console.log('port: %s', port);
console.log('ip: %s', ip);
console.log('---RUN---');

app.post('/', async function (req, res) {
    let body = req.body;
    console.log(JSON.stringify(body));
    let {token, message} = body;
    let testSend = await myrequest.sendMessageToBotParty(token, message);
    res.setHeader('Content-Type', 'application/json');
    res.json(testSend);
});


// error handling
app.use(function(err, req, res, next){
    console.error(err.stack);
    res.status(500).send('Something bad happened!');
});

app.listen(port,()=>{
    console.log('app listening on http://%s:%s', ip, port);
});

module.exports = app ;
